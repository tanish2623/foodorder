package com.tansih.foodorder;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.eightbitlab.bottomnavigationbar.BottomBarItem;
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar;

public class BottomNavigation extends AppCompatActivity{
    BottomNavigationBar bottomNavigationBar;
    int selectedPosition=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation);
        bottomNavigationBar=findViewById(R.id.bottom_bar);

        BottomBarItem item = new BottomBarItem(R.drawable.home, R.string.home);
        BottomBarItem item1 = new BottomBarItem(R.drawable.order, R.string.order);
        BottomBarItem item2 = new BottomBarItem(R.drawable.notification, R.string.notifaction);
        BottomBarItem item3 = new BottomBarItem(R.drawable.avatar, R.string.account);

        addFragment(new FragmentHome());
        bottomNavigationBar.addTab(item);
        bottomNavigationBar.addTab(item1);
        bottomNavigationBar.addTab(item2);
        bottomNavigationBar.addTab(item3);

        bottomNavigationBar.setOnSelectListener(new BottomNavigationBar.OnSelectListener() {
            @Override
            public void onSelect(int position) {
                switch (position){
                    case 0:
                    {
                        if (selectedPosition!=0) {
                            replaceFragment(new FragmentHome());
                            selectedPosition=0;
                        }
                        break;
                    }
                    case 1:
                    {
                        if (selectedPosition!=1) {
                            replaceFragment(new FragmentOrder());
                            selectedPosition = 1;
                        }
                        break;
                    }
                    case 2:
                    {
                        if (selectedPosition!=2) {
                            replaceFragment(new fragmentnotify());
                            selectedPosition = 2;
                        }
                        break;
                    }
                    case 3:
                    { if (selectedPosition!=3) {
                        replaceFragment(new fragmentProfile());
                        selectedPosition = 3;
                    }
                        break;
                    }
                }
            }
        });
    }

    private void addFragment(Fragment fragment) {
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.frameLayout,fragment);
        transaction.commit();
    }

    void replaceFragment(Fragment fragment){
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayout,fragment);
        transaction.commit();
    }


}
