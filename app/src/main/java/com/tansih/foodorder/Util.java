package com.tansih.foodorder;

import android.app.ProgressDialog;
import android.content.Context;

public class Util {
    private static ProgressDialog progressDialog;
    public static void showDialog(Context context)
    {
        if (progressDialog!=null)
        progressDialog.show();
        else
            dialog(context).show();
    }

    private static ProgressDialog dialog(Context context) {
        progressDialog=new ProgressDialog(context);
        progressDialog.setMessage("wait a sec...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        return progressDialog;
    }

    public static void dismisDialog(){
        progressDialog.dismiss();
    }
}
