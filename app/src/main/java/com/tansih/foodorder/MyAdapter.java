package com.tansih.foodorder;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyHolder> {
    Restaurent restaurent;
    Context context;
    public MyAdapter(Context activity) {
        restaurent=new Restaurent();
        this.context=activity;
    }

    @NonNull
    @Override
    public MyAdapter.MyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view= LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.viewlist,viewGroup,false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyHolder myHolder, final int i) {
        myHolder.name.setText(restaurent.names[i]);
        Picasso.get().load(restaurent.images[i])
                .into(myHolder.image);
        myHolder.type.setText(restaurent.type[i]);
        myHolder.off.setText(restaurent.off[i]);
        myHolder.two.setText(restaurent.two[i]);
        myHolder.time.setText(restaurent.time[i]);
        myHolder.rating.setText(restaurent.rating[i]);
        myHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,FoodItems.class);
                intent.putExtra("type",restaurent.names[i]);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return restaurent.images.length;
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView image;
        TextView type;
        TextView off;
        TextView two;
        TextView time;
        TextView rating;
        CardView cardView;
        public MyHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            image=itemView.findViewById(R.id.img);
            type=itemView.findViewById(R.id.type);
            off=itemView.findViewById(R.id.off);
            two=itemView.findViewById(R.id.two);
            time=itemView.findViewById(R.id.time);
            rating=itemView.findViewById(R.id.rating);
            cardView=itemView.findViewById(R.id.card1);
        }
    }
}
