package com.tansih.foodorder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.Holder> {
    Product product;
    Context context;
    ArrayList<Card>list;

    public ItemAdapter(Context context) {
        this.context=context;
        list=new ArrayList<>();
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.itemview,parent,false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, final int i) {
        holder.itemName.setText(product.getNames()[i]);
        holder.itemCategory.setText(product.getCategory()[i]);
        holder.itemCost.setText("Rs."+product.getCost()[i]);
        Picasso.get().load(product.getImages()[i])
                .into(holder.itemImage);
        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.add.getText().equals("ADD")) {
                    Card card = new Card(product.getNames()[i],
                            product.getNames()[i]+"and"+product.getCost()[i],
                            product.getCategory()[i],
                            product.cost[i],
                            1);
                    list.add(card);
                    Toast.makeText(context,"Added to Cart", Toast.LENGTH_SHORT).show();
                    holder.add.setText("REMOVE");
                }else
                {
                   int index= removeItem(product.getNames()[i]+"and"+product.getCost()[i]);

                    list.remove(index);
                    holder.add.setText("ADD");

                }
            }
        });
    }

    private int removeItem(String s) {
        for (int i=0; i<list.size();i++)
        {
            if (s.equals(list.get(i).order_number));
            {
                return i;
            }

        }
        return -1;
    }

    @Override
    public int getItemCount() {
        return product.images.length;
    }

    public void data(Product product) {
        this.product=product;
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView itemName;
        ImageView itemImage;
        TextView itemCategory;
        TextView itemCost;
        Button add;
        public Holder(@NonNull View itemView) {
            super(itemView);
            itemName=itemView.findViewById(R.id.itemName);
            itemImage=itemView.findViewById(R.id.itemImage);
            itemCategory=itemView.findViewById(R.id.itemCategory);
            itemCost=itemView.findViewById(R.id.itemCost);
            add=itemView.findViewById(R.id.add);

        }
    }
    interface openCart
    {
        void openCart();
    }
}
