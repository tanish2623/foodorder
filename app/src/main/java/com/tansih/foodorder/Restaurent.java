package com.tansih.foodorder;

import java.util.Arrays;
import java.util.List;

public class Restaurent {
    String names[] = {
            "Harbans Ice Cream",
            "Bollyfood",
            "Rangeela Fast Food",
            "Patiala Pizza",
            "Mohan Continental",
            "Red Cafe",
            "Vatsa Food Factory",
            "Mohan Chicken Wala",
            "Param Bakers",
            "Burger House",
            "Patiala Shahi Lassi",
            "Burgrill (Model Town)",
            "Green's Resto Bar",
            "Jungle Junction",
            "Koko's Kitchen",
            "Patialvi's Food Joint",
            "Super Donuts",
            "Sahni Dhaba",
            "Fresh Bytes",
            "Roll Express",

    };
    String images[] = {
            "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/qqn0p01kf6lllpprj9wr",
            "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/m4lzzyd0vuwatfeyiaao̥",
            "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/u4omk3fdfjwtcy0pioza",
            "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/xraezfpygokicohpl10n",
            "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/ihcax22dvvbsy1tk9acg",
            "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/avatgxd5bpym8msd4qcg",
            "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/zlqmg2ycfsiblebeqc1z",
            "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/qsf0wwq28nv9ju1vbzki",
            "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/bfrsmznqyfkdpsclocc3",
            "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/qmg4gunyzz6yfxhroriz",
            "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/dzxmailvzmb9t8lhx4ko",
            "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/rvvp92m14vrjfjtu5uyp",
            "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/uiqdnsyfcehreswgl1fy",
            "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/ftwg1tniarmciop0x1jy",
            "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/zs89bqvybalzvku05h94",
            "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/frx2y9ke8yyfaks4hers",
            "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/wsaxemmxvqgwkwz3kopy",
            "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/lfminh0usuyrgq4fw9ia",
            "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/ziqfmhl2zbyj1j0kvxy4",
            "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/bcyupl3jojkshqypnpbz",

    };

    String type[] = {
            "Desserts",
            "North Indian",
            "Fast Food",
            "Pizzas",
            "North Indian, Continental",
            "Fast Food",
            "Fast Food, Pizzas",
            "North Indian",
            "Desserts",
            "Fast Food",
            "Beverages, Desserts",
            "Fast Food, Beverages",
            "North Indian",
            "Continental",
            "Cafe, Italian",
            "North Indian, Chinese, Continental",
            "Desserts",
            "North Indian",
            "Fast Food",
            "Chinese",


    };
    String time[] = {
            "20 mins",
            "15 mins",
            "28 mins",
            "25 mins",
            "10 mins",
            "30 mins",
            "25 mins",
            "26 mins",
            "29 mins",
            "24 mins",
            "27 mins",
            "17 mins",
            "18 mins",
            "23 mins",
            "22 mins",
            "24 mins",
            "19 mins",
            "16 mins",
            "19 mins",
            "15 mins",

    };
    String rating[]={
            "3.8",
            "4.8",
            "4.1",
            "3.1",
            "4.2",
            "2.8",
            "3.9",
            "3.6",
            "4.9",
            "2.9",
            "4.3",
            "4.2",
            "3.7",
            "4.2",
            "5.0",
            "3.5",
            "4.3",
            "2.9",
            "2.5",
            "4.8",

    };
    String two[]={
            "150/- for two",
            "250/- for two",
            "400/- for two",
            "650/- for two",
            "200/- for two",
            "150/- for two",
            "190/- for two",
            "450/- for two",
            "750/- for two",
            "350/- for two",
            "250/- for two",
            "850/- for two",
            "250/- for two",
            "550/- for two",
            "350/- for two",
            "150/- for two",
            "650/- for two",
            "850/- for two",
            "350/- for two",
            "250/- for two",

    };
    String off[]={
            "40% off",
            "50% off",
            "10% off",
            "20% off",
            "40% off",
            "50% off",
            "50% off",
            "30% off",
            "50% off",
            "40% off",
            "20% off",
            "30% off",
            "60% off",
            "50% off",
            "50% off",
            "10% off",
            "50% off",
            "40% off",
            "20% off",
            "30% off",

    };

    public List<String> getNames()
    {
        return Arrays.asList(names);
    }
    public List<String> getImages()
    {
        return Arrays.asList(images);
    }
    public List<String> getTypes()
    {
        return Arrays.asList(type);
    }
    public List<String> getDescription()
    {
        return Arrays.asList(time);
    }
}
