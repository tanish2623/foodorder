package com.tansih.foodorder;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

public class RegisterPage extends Activity {
    EditText etmail;
    Button signup;
    EditText password;
    EditText phnumber;
    String mail;
    String pass;
    EditText firstName;
    EditText lastName;
    String frstname;
    String phno;
    String lstname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registerform);
        etmail=findViewById(R.id.email);
        password=findViewById(R.id.password);
        signup=findViewById(R.id.signup);
        phnumber=findViewById(R.id.phoneno);
        firstName=findViewById(R.id.fname);
        lastName=findViewById(R.id.lname);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate())
                {
                    doRegister();
                }
            }
        });

    }

    private void doRegister() {
        final User user=new User();
        user.setFirstName(frstname);
        user.setEmail(mail);
        user.setLastName(lstname);
        user.setPhoneno(phno);
        user.setPassword(pass);
        Util.showDialog(this);
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(mail,pass)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser fUser=task.getResult().getUser();
                            savetoDatabase(user,fUser);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Util.dismisDialog();
                Toast.makeText(RegisterPage.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void savetoDatabase(User user, FirebaseUser fUser) {
        FirebaseFirestore.getInstance().collection("users")
                .document(fUser.getUid()).set(user)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful())
                        {
                            Util.dismisDialog();
                            Toast.makeText(RegisterPage.this, "User Registerd Succesfully", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(RegisterPage.this,BottomNavigation.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Util.dismisDialog();
                Toast.makeText(RegisterPage.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean validate() {
        mail = etmail.getText().toString();
        pass = password.getText().toString();
        frstname = firstName.getText().toString();
        lstname=lastName.getText().toString();
        phno=phnumber.getText().toString();
        if (mail.isEmpty() || pass.isEmpty()|| frstname.isEmpty() || lstname.isEmpty()|| phno.isEmpty())
            return false;
        else
            return  true;
    }
}

