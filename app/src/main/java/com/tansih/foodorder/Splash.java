package com.tansih.foodorder;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Splash extends AppCompatActivity {
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        textView=findViewById(R.id.textview);
        Animation myanim= AnimationUtils.loadAnimation(this,R.anim.mytransition);
        textView.startAnimation(myanim);

        final FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (user!=null)
                    startActivity(new Intent(Splash.this,BottomNavigation.class));
                else
                    startActivity(new Intent(Splash.this,Login.class));
                finish();

            }
        }, 2000);
    }
}
