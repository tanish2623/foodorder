package com.tansih.foodorder;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Date;

public class Cart extends AppCompatActivity {
    Button order;
    TextView order1;
    Orders orders;
    String shopName;
    CartAdapter adapter;
    String image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        order = findViewById(R.id.placeorder);
        order1 = findViewById(R.id.textorder);
        String list = getIntent().getStringExtra("list");
        shopName = getIntent().getStringExtra("shopName");

        orders = new Gson().fromJson(list, Orders.class);

        adapter = new CartAdapter(orders);
        RecyclerView recyclerView = findViewById(R.id.cart_recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        if (orders.list.size() == 0) {
            order.setVisibility(View.GONE);
            order1.setVisibility(View.VISIBLE);
        } else {
            order.setVisibility(View.VISIBLE);
            order1.setVisibility(View.GONE);
        }
        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                placeOrder();
            }
        });
    }

    private void placeOrder() {
        Booking booking=new Booking();
        booking.setShopName(shopName);
        booking.setTimestemp(new Date().getTime());
        booking.setUid(FirebaseAuth.getInstance().getCurrentUser().getUid());
        booking.setItems(adapter.orders.getList());
        booking.setTotalPrice(getTotalPrice(adapter.orders.getList()));
        Util.showDialog(this);
        FirebaseFirestore.getInstance().collection("orders")
                .document().set(booking)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Util.dismisDialog();
                            Toast.makeText(Cart.this, "order Placed", Toast.LENGTH_SHORT).show();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Util.dismisDialog();
                Toast.makeText(Cart.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String getTotalPrice(ArrayList<Card> list) {
        int price=0;
        for (Card card : list)
        {
            price=price+Integer.parseInt(card.getPrice());
        }
        return String.valueOf(price);
    }
}
