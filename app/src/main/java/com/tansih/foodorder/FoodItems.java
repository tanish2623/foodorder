package com.tansih.foodorder;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

public class FoodItems extends AppCompatActivity {
RecyclerView recyclerView;
    ItemAdapter itemAdapter;
    FloatingActionButton actionButton;
    String shopName;
    String image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_items);
        actionButton=findViewById(R.id.cartbutton);

        String name=getIntent().getStringExtra("type");
        recyclerView=findViewById(R.id.itemrecycler);
        RecyclerView.LayoutManager manager= new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(manager);

        itemAdapter = new ItemAdapter(this);
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Orders orders=new Orders();
                orders.setList(itemAdapter.list);
                String list= new Gson().toJson(orders);
                Intent intent=new  Intent(FoodItems.this,Cart.class);
                intent.putExtra("list",list);
                intent.putExtra("shopName",shopName);
                intent.putExtra("image",shopName);
                startActivity(intent);
            }
        });

        setData(name);
    }

    private void setData(String name) {
        ImageList imageList=new ImageList();
        NameList nameList=new NameList();
        CategoryList categoryList=new CategoryList();
        CostList costList=new CostList();
        switch (name)
        {

            case "Harbans Ice Cream":
            {
                shopName="Harbans Ice Cream";
                image=imageList.HarbansImage[0];
                Product product=new Product();
                product.setImages(imageList.HarbansImage);
                product.setNames(nameList.HarbansName);
                product.setCategory(categoryList.HarbansCategory);
                product.setCost(costList.HarbansCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;
            }
            case "Bollyfood":
            {
                shopName="Bollyfood";
                Product product=new Product();
                product.setImages( imageList.BollyFoodImage);
                product.setNames( nameList.BollyFoodName);
                product.setCategory(categoryList.BollyFoodCategory);
                product.setCost(costList.BollyFoodCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;
            }
            case "Rangeela Fast Food":
            {
               /* Product product=new Product();
                product.setImages( imageList.RangeelaFastFoodImage);
                product.setNames( nameList.RangeelaFastFoodName);
                product.setCategory(categoryList.RangeelaFastFoodCategory);
                product.setCost(costList.RangeelaFastFoodCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;*/
                Product product=new Product();
                product.setImages( imageList.BollyFoodImage);
                product.setNames( nameList.BollyFoodName);
                product.setCategory(categoryList.BollyFoodCategory);
                product.setCost(costList.BollyFoodCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;
            }
            case  "Patiala Pizza":
            {
                /*Product product=new Product();
                product.setImages( imageList.PatialaPizzaImage);
                product.setNames( nameList.PatialaPizzaName);
                product.setCategory(categoryList.PatialaPizzaCategory);
                product.setCost(costList.PatialaPizzaCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;
                */
                Product product=new Product();
                product.setImages( imageList.BollyFoodImage);
                product.setNames( nameList.BollyFoodName);
                product.setCategory(categoryList.BollyFoodCategory);
                product.setCost(costList.BollyFoodCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;
            }
            case "Mohan Continental":
            {
                Product product=new Product();
                product.setImages( imageList.MohanContinentalImage);
                product.setNames( nameList.MohanContinentalName);
                product.setCategory(categoryList.MohanContinentalCategory);
                product.setCost(costList.MohanContinentalCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;
            }
            case "Red Cafe":
            {
                Product product=new Product();
                product.setImages( imageList.BollyFoodImage);
                product.setNames( nameList.BollyFoodName);
                product.setCategory(categoryList.BollyFoodCategory);
                product.setCost(costList.BollyFoodCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;
            }
            case "Vatsa Food Factory":
            {
                Product product=new Product();
                product.setImages( imageList.BollyFoodImage);
                product.setNames( nameList.BollyFoodName);
                product.setCategory(categoryList.BollyFoodCategory);
                product.setCost(costList.BollyFoodCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;
            }
            case "Mohan Chicken Wala":
            {
                Product product=new Product();
                product.setImages( imageList.BollyFoodImage);
                product.setNames( nameList.BollyFoodName);
                product.setCategory(categoryList.BollyFoodCategory);
                product.setCost(costList.BollyFoodCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;
            }
            case "Param Bakers":
            {
                Product product=new Product();
                product.setImages( imageList.BollyFoodImage);
                product.setNames( nameList.BollyFoodName);
                product.setCategory(categoryList.BollyFoodCategory);
                product.setCost(costList.BollyFoodCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;
            }
            case "Burger House":
            {
                Product product=new Product();
                product.setImages( imageList.BollyFoodImage);
                product.setNames( nameList.BollyFoodName);
                product.setCategory(categoryList.BollyFoodCategory);
                product.setCost(costList.BollyFoodCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;
            }
            case "Patiala Shahi Lassi":
            {
                Product product=new Product();
                product.setImages( imageList.PatialaShahiLassiImage);
                product.setNames( nameList.PatialaShahiLassiName);
                product.setCategory(categoryList.PatialaShahiLassiCategory);
                product.setCost(costList.PatialaShahiLassiCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;
            }
            case "Burgrill (Model Town)":
            {
                Product product=new Product();
                product.setImages( imageList.BurgrillImage);
                product.setNames( nameList.BurgrillName);
                product.setCategory(categoryList.BurgrillCategory);
                product.setCost(costList.BurgrillCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;
            }
            case "Green's Resto Bar":
            {
                Product product=new Product();
                product.setImages( imageList.GreenRestoBarImage);
                product.setNames( nameList.GreenRestoBarName);
                product.setCategory(categoryList.GreenRestoBarCategory);
                product.setCost(costList.GreenRestoBarCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;
            }
            case "Jungle Junction":
            {
                Product product=new Product();
                product.setImages( imageList.BollyFoodImage);
                product.setNames( nameList.BollyFoodName);
                product.setCategory(categoryList.BollyFoodCategory);
                product.setCost(costList.BollyFoodCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;
            }
            case "Koko's Kitchen":
            {
                Product product=new Product();
                product.setImages( imageList.KokoKitchenImage);
                product.setNames( nameList.KokoKitchenName);
                product.setCategory(categoryList.KokoKitchenCategory);
                product.setCost(costList.KokoKitchenCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;
            }
            case "Patialvi's Food Joint":
            {
                Product product=new Product();
                product.setImages( imageList.BollyFoodImage);
                product.setNames( nameList.BollyFoodName);
                product.setCategory(categoryList.BollyFoodCategory);
                product.setCost(costList.BollyFoodCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;
            }
            case "Super Donuts":
            {
                Product product=new Product();
                product.setImages( imageList.SuperDonutsImage);
                product.setNames( nameList.SuperDonutsName);
                product.setCategory(categoryList.SuperDonutsCategory);
                product.setCost(costList.SuperDonutsCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;
            }
            case "Sahni Dhaba":
            {
                Product product=new Product();
                product.setImages( imageList.BollyFoodImage);
                product.setNames( nameList.BollyFoodName);
                product.setCategory(categoryList.BollyFoodCategory);
                product.setCost(costList.BollyFoodCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;
            }
            case "Fresh Bytes":
            {
                Product product=new Product();
                product.setImages( imageList.BollyFoodImage);
                product.setNames( nameList.BollyFoodName);
                product.setCategory(categoryList.BollyFoodCategory);
                product.setCost(costList.BollyFoodCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;
            }
            case "Roll Express":
            {
                Product product=new Product();
                product.setImages( imageList.BollyFoodImage);
                product.setNames( nameList.BollyFoodName);
                product.setCategory(categoryList.BollyFoodCategory);
                product.setCost(costList.BollyFoodCost);
                itemAdapter.data(product);
                recyclerView.setAdapter(itemAdapter);
                break;
            }


        }
    }
}
