
package com.tansih.foodorder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public  class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyCartAdapter> {

    Orders orders;

    public CartAdapter(Orders orders) {
        this.orders = orders;
    }


    @NotNull
    public CartAdapter.MyCartAdapter onCreateViewHolder(@NotNull ViewGroup p0, int p1) {
        View view = LayoutInflater.from(p0.getContext()).inflate(R.layout.item_cart, p0, false);
        return new MyCartAdapter(view);
    }


    public int getItemCount() {
        return orders.list.size();
    }

    public void onBindViewHolder(final MyCartAdapter holder, final int position) {
        final View itemView = holder.itemView;
        final Card card = orders.list.get(position);
        TextView textname = itemView.findViewById(R.id.cart_item_name);
        TextView note = itemView.findViewById(R.id.cart_item_desc);

        textname.setText(card.getName());
        holder.textCount.setText(String.valueOf(orders.list.get(position).count));
        note.setText(card.getNote());
        holder.cart_increment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count=card.getCount()+1;
                updateValue(count, holder.textCount, position);
                orders.list.get(position).setCount(count);
            }
        });

        holder.cart_dec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (card.getCount() > 1) {
                    int count=card.getCount()-1;
                    updateValue(count, holder.textCount, position);
                    orders.list.get(position).setCount(count);
                }
            }
        });



        holder.cart_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orders.list.remove(position);
                notifyItemRemoved(position);
            }
        });
    }

    public void updateValue(int real_count, TextView textCount, int val) {
        String c=String.valueOf(real_count);
        textCount.setText(c);

    }


    public final class MyCartAdapter extends RecyclerView.ViewHolder {
        TextView textCount;
        TextView cart_remove;
        TextView cart_dec;
        TextView cart_increment;

        public MyCartAdapter(@NotNull View view) {
            super(view);
            textCount = itemView.findViewById(R.id.cart_item_count);
            cart_remove = itemView.findViewById(R.id.cart_item_remove);
            cart_dec = itemView.findViewById(R.id.card_item_decrement);
            cart_increment = itemView.findViewById(R.id.card_item_increment);
        }
    }
}

