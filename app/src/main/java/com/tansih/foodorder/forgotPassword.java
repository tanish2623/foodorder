package com.tansih.foodorder;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class forgotPassword extends AppCompatActivity {
TextView register;
Button button;
EditText email;
String mail;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        register=findViewById(R.id.etregister);
        button=findViewById(R.id.forgotButton);
        email=findViewById(R.id.etMail1);
        progressDialog=new ProgressDialog(this);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validate())
                {
                    doforgotpass();
                }
            }
        });
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(forgotPassword.this, RegisterPage.class);
                startActivity(intent);
            }
        });
    }

    private void doforgotpass() {

            Util.showDialog(this);
            FirebaseAuth.getInstance().sendPasswordResetEmail(mail)
            .addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful())
                    {
                        email.setText("");
                        Toast.makeText(forgotPassword.this, "Check your Mail inbox To reset Password", Toast.LENGTH_LONG).show();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Util.dismisDialog();
                    Toast.makeText(forgotPassword.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

    }

    private boolean validate() {
        mail = email.getText().toString();
        if (mail.isEmpty())
            return false;
        else
            return  true;
    }
}
