package com.tansih.foodorder;

import java.util.ArrayList;

public class Booking {
    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Long getTimestemp() {
        return timestemp;
    }

    public void setTimestemp(Long timestemp) {
        this.timestemp = timestemp;
    }

    public ArrayList<Card> getItems() {
        return items;
    }

    public void setItems(ArrayList<Card> items) {
        this.items = items;
    }

    String shopName;
    String totalPrice;
    String uid;
    Long timestemp;
    ArrayList<Card>items;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    String image;

}
