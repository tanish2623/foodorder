package com.tansih.foodorder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

public class orderHistoryadapter extends RecyclerView.Adapter<orderHistoryadapter.Holder> {
    Context context;
    public orderHistoryadapter(Context context) {
        this.context=context;
    }

    @NonNull
    @Override
    public orderHistoryadapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ordervieww,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull orderHistoryadapter.Holder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 2 ;
    }

    public class Holder extends RecyclerView.ViewHolder {
        public Holder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
