package com.tansih.foodorder;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Login extends AppCompatActivity {
TextView newuser;
EditText etMail;
EditText pass;
Button button;
TextView forgotpass;
    String mail;
    String password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etMail=findViewById(R.id.etMail1);
        pass=findViewById(R.id.pass);
        forgotpass=findViewById(R.id.forgot);
        newuser=findViewById(R.id.newuser);
        button=findViewById(R.id.Button);

        newuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Login.this, RegisterPage.class);
                startActivity(intent);
                finish();
            }

        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                        if (validate())
                        {
                            doLogin();
                        }


            }
        });
        forgotpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Login.this,forgotPassword.class);
                startActivity(intent);
            }
        });


    }

    private void doLogin() {
        Util.showDialog(this);
        FirebaseAuth.getInstance().signInWithEmailAndPassword(mail,password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Util.dismisDialog();
                        if (task.isSuccessful()) {
                            Toast.makeText(Login.this, "Login successful", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(Login.this, BottomNavigation.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Util.dismisDialog();
                Toast.makeText(Login.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean validate() {
        mail = etMail.getText().toString();

        password = pass.getText().toString();
        if (mail.isEmpty() || password.isEmpty())
            return false;
        else
            return  true;
    }

}
